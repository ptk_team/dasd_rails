# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180311175951) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contents", force: :cascade do |t|
    t.string "name"
  end

  create_table "departments", force: :cascade do |t|
    t.string "name"
  end

  create_table "disks", force: :cascade do |t|
    t.bigint "department_id"
    t.bigint "content_id"
    t.bigint "software_id"
    t.bigint "owner_id"
    t.integer "unit"
    t.string "volume"
    t.integer "capacity"
    t.decimal "utilization", precision: 3
    t.integer "pprc_secondary"
    t.string "dtap"
    t.date "added_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "group_id"
    t.index ["content_id"], name: "index_disks_on_content_id"
    t.index ["department_id"], name: "index_disks_on_department_id"
    t.index ["group_id"], name: "index_disks_on_group_id"
    t.index ["owner_id"], name: "index_disks_on_owner_id"
    t.index ["software_id"], name: "index_disks_on_software_id"
    t.index ["unit"], name: "index_disks_on_unit", unique: true
  end

  create_table "disks_systems", id: false, force: :cascade do |t|
    t.bigint "disk_id"
    t.bigint "system_id"
    t.index ["disk_id"], name: "index_disks_systems_on_disk_id"
    t.index ["system_id"], name: "index_disks_systems_on_system_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "capacity"
    t.decimal "utilization", precision: 3
  end

  create_table "owners", force: :cascade do |t|
    t.string "name"
  end

  create_table "softwares", force: :cascade do |t|
    t.string "name"
    t.string "company"
  end

  create_table "systems", force: :cascade do |t|
    t.string "name"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

end
