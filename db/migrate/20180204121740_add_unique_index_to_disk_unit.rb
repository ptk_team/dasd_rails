class AddUniqueIndexToDiskUnit < ActiveRecord::Migration[5.1]
  def change
    remove_index :disks, :unit
    add_index :disks, :unit, unique: true
  end
end
