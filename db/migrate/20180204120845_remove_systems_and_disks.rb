class RemoveSystemsAndDisks < ActiveRecord::Migration[5.1]
  def change
    drop_table :systems_disks
  end
end
