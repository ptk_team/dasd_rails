class CreateDisks < ActiveRecord::Migration[5.1]
  def change
    create_table :disks do |t|
      t.belongs_to :department
      t.belongs_to :content
      t.belongs_to :software
      t.belongs_to :system
      t.belongs_to :owner

      t.integer :unit
      t.string :volume
      t.integer :capacity
      #scale - number of digits afrer decimal point
      #precision - total numbers
      t.decimal :utilization, :precision => 3, :scale => 0
      t.integer :pprc_secondary
      t.string :dtap
      t.date :added_at

      t.timestamps
    end
  end
end
