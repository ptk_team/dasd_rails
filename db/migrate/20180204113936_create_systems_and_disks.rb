class CreateSystemsAndDisks < ActiveRecord::Migration[5.1]
  def change
    create_table :systems_disks, id: false do |t|
      t.belongs_to :disk, index: true
      t.belongs_to :system, index: true
    end
  end
end