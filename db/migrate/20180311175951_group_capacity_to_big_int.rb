class GroupCapacityToBigInt < ActiveRecord::Migration[5.1]
  def change
    change_column :groups, :capacity, :bigint
  end
end
