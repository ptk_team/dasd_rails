class CreateDisksAndSystems < ActiveRecord::Migration[5.1]
  def change
    create_table :disks_systems, id: false do |t|
      t.belongs_to :disk, index: true
      t.belongs_to :system, index: true
    end
  end
end