class RemoveCreatedAtUpdatedAt < ActiveRecord::Migration[5.1]
  def change
    remove_column :systems, :created_at
    remove_column :systems, :updated_at

    remove_column :softwares, :created_at
    remove_column :softwares, :updated_at


    remove_column :owners, :created_at
    remove_column :owners, :updated_at

    remove_column :departments, :created_at
    remove_column :departments, :updated_at

    remove_column :contents, :created_at
    remove_column :contents, :updated_at
  end
end
