class AddIndexToDisks < ActiveRecord::Migration[5.1]
  def change
    add_index :disks, :unit
  end
end
