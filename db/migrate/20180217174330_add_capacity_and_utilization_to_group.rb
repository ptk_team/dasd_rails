class AddCapacityAndUtilizationToGroup < ActiveRecord::Migration[5.1]
  def change
    add_column :groups, :capacity, :integer
    add_column :groups, :utilization, :decimal, :precision => 3, :scale => 0
  end
end
