class RemoveSystemAssociationFromDisks < ActiveRecord::Migration[5.1]
  def change
    remove_reference(:disks, :system, index: true)
  end
end
