class AddStorageGroupsToDisk < ActiveRecord::Migration[5.1]
  def change
    add_reference :disks, :group, index: true
  end
end
