class GroupsController < ApplicationController
  before_action :take_group, only: :show

  def index
      respond_with(@groups = Group.paginate(:page => params[:page], :per_page => 30))
    end

  def show
    respond_with(@group)
  end

  private
  def group_params
    params.require(:group).permit(:name)
  end

  def take_group
    @group = Group.find(params[:id])
  end
end