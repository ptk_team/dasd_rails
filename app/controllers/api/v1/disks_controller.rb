class Api::V1::DisksController < ApplicationController
  #!!!! create with doorkeeper
  skip_before_action :verify_authenticity_token
  respond_to :json

  def show
    respond_with(Disk.find(params[:id]))
  end

  def report
    params.delete :disk
    # disks = params[:report].to_h
    head :ok
  end

  private

  def question_params
    params.require(:report).permit(:date_time, :system, disks: [:unit, :volume, :capacity, :utilization, :storgroup])
  end
end