class Api::V1::ReportsController < ApplicationController
  #!!!! create with doorkeeper
  skip_before_action :verify_authenticity_token
  respond_to :json

  def show
    respond_with(Disk.find(params[:id]))
  end

  def report
    disks = report_params[:disks]
    system = report_params[:system]
    disks.each do |disk|
      real_disk = Disk.find_by unit: disk[:unit].hex
      real_disk ||= Disk.new
      # real_disk.update_attributes(volume: disk.volume, utilization: disk.utilization)
      real_disk.report(system, disk)
      real_disk.save
    end
    head :ok
  end

  private

  def report_params
    params.require(:report).permit(:date_time, :system, disks: [:unit, :volume, :capacity, :utilization, :storgroup])
  end
end