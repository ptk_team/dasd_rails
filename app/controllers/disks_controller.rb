class DisksController < ApplicationController
  before_action :take_disk, only: [:show, :destroy, :update]

  respond_to :xls, :xlsx

  def index
    # respond_with(@disks = Disk.all)

    if request.format == :html
      respond_with(@disks = Disk.paginate(:page => params[:page], :per_page => 30))
    else
      respond_with(@disks=Disk.all)
    end

    # respond_to do |format|
    #   format.html { render :html => @disks = Disk.paginate(:page => params[:page], :per_page => 30) }# show.html.erb
    #   format.xls  { render :xls => @disks = Disk.all }
    # end

  end

  def new
    respond_with(@disk = Disk.new)
  end

  def create
    @disk = Disk.create(disk_params)
    respond_with(@disk)
  end

  def update
    @disk.update(disk_params)
    respond_with(@disk)
  end

  def destroy
    respond_with(@disk.destroy)
  end

  def show
    respond_with(@disk)
  end

  def import
    Disk.import(params[:file])
    redirect_to root_url, notice: 'Disks imported'
  end

  private
  def disk_params
    params.require(:disk).permit(:unit, :volume, :group_id, :capacity, :utilization,
                                 :pprc_secondary, :dtap, :added_at, :department_id,
                                 :content_id, :software_id, :owner_id, { system_ids:[]})
  end

  def take_disk
    @disk = Disk.find(params[:id])
  end
end
