class Owner < ApplicationRecord
  has_one :disk
  validates :name, uniqueness: true
end
