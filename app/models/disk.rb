class Disk < ApplicationRecord
  DTAP = ['development', 'test', 'acceptance', 'production']

  belongs_to :content
  belongs_to :department
  belongs_to :owner
  belongs_to :software
  belongs_to :group
  has_and_belongs_to_many :systems, dependent: :destroy
  # Test it
  has_paper_trail on: [:update], only: [:volume, :utilization]
  default_scope {order(unit: :asc)}

  after_create :calculate_group_stats
  after_update :calculate_group_stats, if: :changed_disk_stats?
  # duplicated CALLBACKS overrides themselves
  # after_update :calculate_group_stats, if: :saved_change_to_capacity?
  # after_update :calculate_group_stats, if: :saved_change_to_group_id?
  # after_update :calculate_old_group_stats, if: :saved_change_to_group_id?

  #################
  # Default methods
  #################

  def empty?
    self.volume[0..1] == 'TM'
  end

  def unit_to_hex
    self.unit.to_s(16)
  end

  def to_chart_array
    [self.updated_at, self.utilization]
  end

  def custom_capacity(units)
    case units
      when "MB"
        self.capacity / 1024
      when "GB"
        self.capacity / 1048576
      else
        self.capacity
    end
  end

  def changed_disk_stats?
    saved_change_to_utilization? || saved_change_to_capacity? || saved_change_to_group_id?
  end

  def calculate_group_stats
    calculate_old_group_stats if saved_change_to_group_id?
    if self.group && self.group.name != 'NOGROUP'
      self.group.capacity = self.group.disks.map {|d| d.capacity}.sum
      if self.group.capacity != 0
        used_kb = self.group.disks.map {|d| d.capacity * d.utilization / 100}.sum
        self.group.utilization = (used_kb * 100) / self.group.capacity
      else
        self.group.utilization = 0
      end
      self.group.save!
    end
  end

  # make tests!
  def calculate_old_group_stats
    if self.group_id_was
      old_group = Group.find(self.group_id_was)
      if old_group.name != 'NOGROUP'
        old_group.capacity = old_group.disks.map {|d| d.capacity}.sum
        if old_group.capacity != 0
          used_kb = old_group.disks.map {|d| d.capacity * d.utilization / 100}.sum
          old_group.utilization = (used_kb * 100) / old_group.capacity
        else
          old_group.utilization = 0
        end
        old_group.save!
      end
    end
  end

  def report(sysname, disk)
    # Creates new disk if no exists
    # Add disk systems relation if no exists
    #
    #
    # CREATE RELATED RESOURCES CHECK

    self.unit = disk[:unit].hex if self.new_record?
    self.attributes = {
        volume: disk[:volume],
        capacity: disk[:capacity],
        utilization: disk[:utilization]
    }

    # OWNER
    self.owner = Owner.where(name: 'NOOWNER').first || Owner.create!(name: 'NOOWNER') unless self.owner

    # SOFTWARE
    self.software = Software.where(name: 'NOSOFTWARE').first || Software.create!(name: 'NOSOFTWARE') unless self.software

    # CONTENT
    self.content = Content.where(name: 'NOCONTENT').first || Content.create!(name: 'NOCONTENT') unless self.content

    # DEPARTMENT
    self.department = Department.where(name: 'NODEPARTMENT').first || Department.create!(name: 'NODEPARTMENT') unless self.department

    # STORGROUP
    self.group = Group.where(name: 'NOGROUP').first || Group.create!(name: 'NOGROUP')
    unless disk[:storgroup] == ''
      self.group = Group.where(name: disk[:storgroup]).first || Group.create!(name: disk[:storgroup])
    end

    # SYSTEM
    if self.systems.all.map(&:name).exclude? sysname
      system = System.where(name: sysname).first || System.create!(name: sysname)
      self.systems << system
    end
  end

  def self.import2(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      disk = find_by_id(row["id"]) || new
      disk.attributes = row.to_hash.slice(*row.to_hash.keys)
      disk.save!
    end
  end

  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      disk = find_by_id(row["id"]) || new
      # disk.attributes = row.to_hash.slice(*row.to_hash.keys )
      department = Department.where(name: row["department"]).first || Department.create!(name: row["department"])
      content = Content.where(name: row["content"]).first || Content.create!(name: row["content"])
      software = Software.where(name: row["software"]).first || Software.create!(name: row["software"])
      owner = Owner.where(name: row["owner"]).first || Owner.create!(name: row["owner"])
      disk.attributes = {
          id: row["id"],
          unit: row["unit"],
          volume: row["volume"],
          capacity: row["capacity"],
          utilization: row["utilization"],
          pprc_secondary: row["pprc secondary"],
          dtap: row["dtap"],
          added_at: row["added_at"],
          department: department,
          content: content,
          software: software,
          owner: owner
      }
      if row["system"] && disk.systems.all.map(&:name) != row["system"].split
        row["system"].split.each do |sysname|
          system = System.where(name: sysname).first || System.create!(name: sysname)
          disk.systems << system
        end
      end
      disk.save
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
      when ".csv" then
        Roo::Csv.new(file.path, nil, :ignore)
      when ".xls" then
        Roo::Excel.new(file.path, nil, :ignore)
      when ".xlsx" then
        Roo::Excelx.new(file.path)
      else
        raise "Unknown file type: #{file.original_filename}"
    end
  end
end
