class Group < ApplicationRecord
  has_many :disks
  validates :name, uniqueness: true
  has_paper_trail only: [:utilization]

  def to_chart_array
    [self.updated_at, self.utilization]
  end

end
