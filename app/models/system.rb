class System < ApplicationRecord
  #SYSTEM name must be in UPPER case

  has_and_belongs_to_many :disks, dependent: :destroy
  validates :name, uniqueness: true
end
