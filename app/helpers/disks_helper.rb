module DisksHelper
  def utilization_color(disk)
    case disk.utilization
      when 0..70
        "#FFFFFF"
      when 71..90
        "#F0FF00"
      when 91..100
        "#FF0000"
    end
  end

  def capacity_color(disk)
    case disk.capacity
      when 0..4000000
        "#FFFFFF"
      when 4000001..10000000
        "#FF99CC"
      when 10000001..28000000
        "#FFCC99"
      when 28000001..Float::INFINITY
        "#FFCC00"
    end
  end
end
