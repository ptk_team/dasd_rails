module ApplicationHelper
  def utilization_chart(obj)
    if obj.versions.any? && obj.versions.last.reify
      # Change:
      versions = obj.versions.select{|v| v.event != 'create'}
      versions.collect {|v| [v.reify.updated_at, v.reify.utilization]} << obj.to_chart_array
    end
  end
end
