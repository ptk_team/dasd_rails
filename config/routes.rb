Rails.application.routes.draw do
  root 'disks#index'

  resources :disks do
    collection {post :import}
  end

  resources :groups, only: [:index, :show]

  namespace :api, defaults: {format: :json} do
    namespace :v1 do
      match 'reports/report' => 'reports#report', :via => :post
    end
  end
end

